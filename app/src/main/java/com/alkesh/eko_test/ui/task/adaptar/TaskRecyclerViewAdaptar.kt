package com.alkesh.eko_test.ui.task.adaptar

import android.content.Context
import android.graphics.Movie
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.alkesh.eko_test.R
import com.alkesh.eko_test.data.ui_models.TaskModel
import kotlinx.android.synthetic.main.cell_task_recyclerview.view.*

class TaskRecyclerViewAdaptar(val context: Context, val list: List<TaskModel>) :
    RecyclerView.Adapter<TaskViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TaskViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        val model = list[position]
        holder.bind(model)
    }

    override fun getItemCount(): Int = list.size

}

class TaskViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.cell_task_recyclerview, parent, false)) {


    fun bind(model: TaskModel) {
        itemView.tvTitle?.text = model.title
        if (model.completed) {
            itemView.tvStatus?.text = "Completed"
        } else {
            itemView.tvStatus?.text = "In Progress"

        }
    }

}