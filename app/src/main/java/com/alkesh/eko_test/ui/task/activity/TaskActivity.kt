package com.alkesh.eko_test.ui.task.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.alkesh.eko_test.R
import com.alkesh.eko_test.repository.TaskRepository
import com.alkesh.eko_test.ui.task.adaptar.TaskRecyclerViewAdaptar
import com.alkesh.eko_test.viewmodel.TaskViewModel
import kotlinx.android.synthetic.main.activity_main.*

class TaskActivity : AppCompatActivity() {
    lateinit var taskViewModel: TaskViewModel
    lateinit var taskRepository: TaskRepository
    lateinit var adapter: TaskRecyclerViewAdaptar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        setEvents()
    }

    fun init() {
        taskRepository = TaskRepository(applicationContext)
        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel()::class.java)

        /**
         * Injecting Repository
         */
        taskViewModel.taskRepository = taskRepository

        recycleview.setHasFixedSize(false);
        val mLayoutManager = LinearLayoutManager(this)
        recycleview.setLayoutManager(mLayoutManager);

        val mDividerItemDecoration = DividerItemDecoration(
            recycleview.getContext(),
            mLayoutManager.getOrientation()
        )
        recycleview.addItemDecoration(mDividerItemDecoration)
    }

    fun setEvents() {


        taskViewModel.getTasks().observe(this, Observer {
            Log.e("DATATAAT", "" + it.size)
            adapter = TaskRecyclerViewAdaptar(applicationContext, it)
            recycleview.adapter = adapter

        })
        taskViewModel.getIsLoading().observe(this, Observer {
            if (it)
                progressBar.visibility = View.VISIBLE
            else
                progressBar.visibility = View.GONE

        })
    }

}