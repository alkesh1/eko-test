package com.alkesh.eko_test.rest

import com.alkesh.eko_test.data.dto.TaskDto
import retrofit2.Call
import retrofit2.http.GET


interface ApiService {
    @GET("/todos")
    fun listRepos(): Call<List<TaskDto>>
}