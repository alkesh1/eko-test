package com.alkesh.eko_test.rest

import com.alkesh.eko_test.constant.AppConstants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.google.gson.GsonBuilder




object RetrofitService {
    lateinit var retrofit: Retrofit
    fun getInstance(): ApiService {
        val retrofit = getRetrofitInstance()
        val apiService = retrofit.create(ApiService::class.java)
        return apiService
    }

    private fun getRetrofitInstance(): Retrofit {
        val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
        val builder = Retrofit.Builder()
        builder.baseUrl(AppConstants.getBaseURL())
        builder.addConverterFactory(GsonConverterFactory.create(GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
        builder.client(client)

        retrofit = builder.build()
        return retrofit
    }
}