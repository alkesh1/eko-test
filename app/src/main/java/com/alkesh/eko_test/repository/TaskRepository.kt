package com.alkesh.eko_test.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import androidx.room.RoomDatabase
import com.alkesh.eko_test.data.db.AppDatabase
import com.alkesh.eko_test.data.db.dao.TaskDao
import com.alkesh.eko_test.data.db.entity.Task
import com.alkesh.eko_test.data.dto.TaskDto
import com.alkesh.eko_test.data.ui_models.TaskModel
import com.alkesh.eko_test.rest.ApiService
import com.alkesh.eko_test.rest.RetrofitService
import com.alkesh.eko_test.viewmodel.TaskViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class TaskRepository(val context: Context) {
    var tasks = MutableLiveData<List<TaskModel>>()
    var isLoading = MutableLiveData<Boolean>()
    val taskDao = dbIni()
    fun getTask(): MutableLiveData<List<TaskModel>> {
        loadDataFromNetwork()

        return tasks;
    }

    fun getIsLoading(): MutableLiveData<Boolean> {
        return isLoading
    }

    private fun loadDataFromNetwork() {
        isLoading.value=true
        RetrofitService.getInstance().listRepos().enqueue(object : Callback<List<TaskDto>> {
            override fun onFailure(call: Call<List<TaskDto>>, t: Throwable) {
                loadDataFromDatbase()
                isLoading.value=false
            }

            override fun onResponse(call: Call<List<TaskDto>>, response: Response<List<TaskDto>>) {
                if (response.isSuccessful) {
                    taskDao.deleteAll()
                    val newList = ArrayList<TaskModel>()
                    val list = response.body()
                    for (item in list!!) {
                        newList.add(convertDtoIntoViewModel(item))
                        taskDao.add(convertDtoIntoDbModel(item))
                    }
                    tasks.value = newList
                    isLoading.value=false

                } else {
                    loadDataFromDatbase()
                    isLoading.value=false
                }
            }


        })
    }

    private fun loadDataFromDatbase() {
        val list = dbIni().getAll()
        val newList = ArrayList<TaskModel>()
        for (item in list) {
            newList.add(convertDbModelIntoViewModel(item))
        }
        tasks.value = newList
    }

    fun dbIni(): TaskDao {
        val db = Room.databaseBuilder(
            context,
            AppDatabase::class.java, "database-name"
        ).allowMainThreadQueries().build()
        db.taskDao()
        return db.taskDao();

    }

    private fun convertDtoIntoViewModel(taskDto: TaskDto): TaskModel {
        val taskuiModel = TaskModel()
        taskuiModel.completed = taskDto.completed
        taskuiModel.id = taskDto.id
        taskuiModel.userId = taskDto.userId
        taskuiModel.title = taskDto.title
        return taskuiModel
    }

    private fun convertDtoIntoDbModel(taskDto: TaskDto): Task {
        val task = Task(taskDto.id, taskDto.userId, taskDto.title, taskDto.completed)
        return task
    }

    private fun convertDbModelIntoViewModel(task: Task): TaskModel {
        return TaskModel(task.userId, task.id, task.title, task.completed)
    }


}