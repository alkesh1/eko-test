package com.alkesh.eko_test.constant

class AppConstants{
    companion object {
        fun getBaseURL() : String = "https://jsonplaceholder.typicode.com/"
    }
}