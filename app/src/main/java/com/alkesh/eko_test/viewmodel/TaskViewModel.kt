package com.alkesh.eko_test.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.alkesh.eko_test.data.ui_models.TaskModel
import androidx.lifecycle.LiveData
import com.alkesh.eko_test.repository.TaskRepository


class TaskViewModel : ViewModel() {
    lateinit var taskRepository: TaskRepository
    private var tasks = MutableLiveData<List<TaskModel>>()
    private var isLoading = MutableLiveData<Boolean>()

    fun getTasks(): LiveData<List<TaskModel>> {
        tasks = taskRepository.getTask()
        return tasks
    }

    fun getIsLoading(): LiveData<Boolean> {
        isLoading = taskRepository.getIsLoading()
        return isLoading
    }
}