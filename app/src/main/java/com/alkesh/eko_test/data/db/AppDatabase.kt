package com.alkesh.eko_test.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.alkesh.eko_test.data.db.dao.TaskDao
import com.alkesh.eko_test.data.db.entity.Task

@Database(entities = arrayOf(Task::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun taskDao(): TaskDao
}