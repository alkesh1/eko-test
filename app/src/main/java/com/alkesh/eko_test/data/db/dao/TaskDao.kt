package com.alkesh.eko_test.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.alkesh.eko_test.data.db.entity.Task

@Dao
interface TaskDao {
    @Query("SELECT * FROM Task")
    fun getAll(): List<Task>

    @Query("DELETE FROM Task")
    fun deleteAll()

    @Insert
    fun add(users: Task)
}